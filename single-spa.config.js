import { registerApplication, start } from 'single-spa';

registerApplication(
    // Name of our single-spa application
    'home',
    // loadingFunction
    () => import('./src/app1/app1.js'),
    // activityFunction
    location =>
        location.pathname === '' ||
        location.pathname === '/' ||
        location.pathname.startsWith('/home')
);

registerApplication(
    // Name of our single-spa application
    'react',
    // loadingFunction
    () => import('./src/react/react.app.js'),
    // activityFunction
    pathPrefix('/react')
);

registerApplication(
    'navBar',
    () => import('./src/navBar/navBar.app.js').then(module => module.navBar),
    () => true
);

function pathPrefix(prefix) {
    return function(location) {
        return location.pathname.startsWith(prefix);
    };
}

registerApplication(
    'angularJS',
    () => import('./src/angularJS/angularJS.app.js'),
    pathPrefix('/angularJS')
);

registerApplication(
    // Name of our single-spa application
    'contactmanager',
    // loadingFunction
    () => import('./src/contactmanager/contactmanager.app.js'),
    // activityFunction
    pathPrefix('/contactmanager')
);

registerApplication(
    // Name of our single-spa application
    'htm',
    // loadingFunction
    () => import('./src/htm/htm.app.js'),
    // activityFunction
    pathPrefix('/htm')
);

start();
