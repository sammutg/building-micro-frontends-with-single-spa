import React from 'react';
import ReactDOM from 'react-dom';
import singleSpaReact from 'single-spa-react';
import contactmanager from './index';

const reactLifecycles = singleSpaReact({
    React,
    ReactDOM,
    rootComponent: contactmanager,
    domElementGetter
});

export const bootstrap = [reactLifecycles.bootstrap];

export const mount = [reactLifecycles.mount];

export const unmount = [reactLifecycles.unmount];

function domElementGetter() {
    // This is where single-spa will mount our application
    // Make sure there is a div for us to render into
    let el = document.getElementById('contactmanager');
    if (!el) {
        el = document.createElement('div');
        el.id = 'app1';
        document.body.appendChild(el);
    }

    return el;
}
