import singleSpaHtml from 'single-spa-html';

// single-spa-html is a helper library for mounting raw html and web components
// as single-spa applications or parcels.
const htmlLifecycles = singleSpaHtml({
    template:
        '<h1>Hello World!</h1><p>Le plus petit exemple de html ressemble à ceci.</p>',
    domElementGetter
});

export const bootstrap = htmlLifecycles.bootstrap;
export const mount = htmlLifecycles.mount;
export const unmount = htmlLifecycles.unmount;

function domElementGetter() {
    return document.getElementById('htm');
}
